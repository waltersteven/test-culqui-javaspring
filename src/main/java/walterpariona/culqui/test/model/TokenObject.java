package walterpariona.culqui.test.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

//This class (Model) creates the token object received 
//and provides annotations to validate the existence of the fields
public class TokenObject {
	
	@NotBlank
	private String pan;
	@NotNull
	private Integer exp_year; 
	@NotNull
	private Integer exp_month;
	
	public TokenObject() { }
	
	public TokenObject(String pan, Integer exp_year, Integer exp_month) {
		this.pan = pan;
		this.exp_year = exp_year;
		this.exp_month = exp_month;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public Integer getExp_year() {
		return exp_year;
	}

	public void setExp_year(Integer exp_year) {
		this.exp_year = exp_year;
	}

	public Integer getExp_month() {
		return exp_month;
	}

	public void setExp_month(Integer exp_month) {
		this.exp_month = exp_month;
	}

	@Override
	public String toString() {
		return "TokenModel [pan=" + pan + ", exp_year=" + exp_year + ", exp_month=" + exp_month + "]";
	}	
	
	public String tokenFormat() {
		return "tkn_live_" + this.getPan() + "-" + this.getExp_year() + "-" + this.getExp_month();
	}
	
	public String generateBin() {
		return this.getPan().substring(0, 6);
	}
}
