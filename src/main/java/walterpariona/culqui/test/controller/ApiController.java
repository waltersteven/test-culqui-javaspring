package walterpariona.culqui.test.controller;

import java.io.IOException;
import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import walterpariona.culqui.test.model.ResponseObject;
import walterpariona.culqui.test.model.TokenObject;;

//This class handles exposes a REST API with a POST method.
@RestController
@RequestMapping("/api")
public class ApiController {
		
		RequestController req;
		
		@PostMapping("/tokens")
		public ResponseEntity<ResponseObject> getResponse(@Valid @RequestBody TokenObject tokenObject) throws IOException{
			
			//creating Token object
			TokenObject tknModel = new TokenObject(tokenObject.getPan(),
					tokenObject.getExp_year(), tokenObject.getExp_month());
			
			// getting bin (6 first digits of card number)
			String bin = tknModel.generateBin();
			
			// sending bin number to a RequestController
			req = new RequestController();
			String binList = req.getBinList(bin); 
			
			// returns error if GET request to microservice dind't work
			if (binList == null) return ResponseEntity.badRequest().build();
			
			/* getting scheme value as requested,
			this value could be changed since the function receives it as a parameter */
			String keyVal = req.getValueByKey(binList, "scheme");
			
			// creating a Response object with token, brand, and creation_dt as values
			ResponseObject respObj = new ResponseObject(tknModel.tokenFormat(), keyVal);
			// generating date
			respObj.registerDate();
			

			System.out.println("-----------------> " +  respObj.toString());
			// returns success response 
			return new ResponseEntity<ResponseObject>(respObj, HttpStatus.OK);
			
		}
}
