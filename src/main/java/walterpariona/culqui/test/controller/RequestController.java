package walterpariona.culqui.test.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/* NOTE
 * ENDPOINT: http://localhost:8080/api/tokens 
 * */

// Class which handles every request needed in the app from external microservices
public class RequestController {
	public static String address = "https://lookup.binlist.net/";
	
	// GET request to web service
	public String getBinList(String bin) throws IOException {	
		System.out.println("getBinList: " + bin);
		String newadrs = address.concat(bin);
		URL url = new URL(newadrs);
		// connection
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		
		// defining method
		conn.setRequestMethod("GET");
		
		// headers
		conn.setRequestProperty("Accept-Version", "3");
		
		//reading response
		int respCode = conn.getResponseCode();
		System.out.println("respCode: " + respCode);
	    if (respCode == HttpURLConnection.HTTP_OK) { //success
	    	
	    	System.out.println("-----------------> GET REQUEST TO MICROSERVICE (https://lookup.binlist.net/) WAS SUCCESS");
	    	InputStreamReader inpStr = new InputStreamReader(conn.getInputStream());
	        BufferedReader bufRead = new BufferedReader(inpStr);
	        String inputLine;
	        StringBuffer response = new StringBuffer();
	        while ((inputLine = bufRead .readLine()) != null) {
	            response.append(inputLine);
	        } 
	        //close connection
	        bufRead.close();
	        
	        conn.disconnect();
	        // print result
	        return response.toString();
	    } else { //failed
	        System.out.println("-----------------> GET REQUEST TO MICROSERVICE (https://lookup.binlist.net/) DIDN'T WORK");
	        conn.disconnect();
	        return null;
	    }
	}
	
	// functions that deserializes the JSON in a JSONObjeect and extract the value of specified key (e.g.: schemes)
	public String getValueByKey(String JsonString, String key) {
		JsonObject convertedObject = new Gson().fromJson(JsonString, JsonObject.class);
		JsonElement jsonElm = convertedObject.get(key);
		System.out.println("-----------------> Value of specified key in JSON: " + jsonElm);
        	
		return jsonElm.getAsString();
	}
}
